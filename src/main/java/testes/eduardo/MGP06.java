package testes.eduardo;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import testlink.api.java.client.TestLinkAPIResults;

public class MGP06 {

private WebDriver driver;
	
	@Before
	public void setUp() {

		System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://ggirardon.com:8080/GerenciadorPampatec/");
	}
	
	@Test
	  public void testeCampoRelacaoComClientes() throws Exception {
	    driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
	    driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("huillian15@gmail.com");
	    driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
	    driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("oioioi");
	    driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
	    driver.findElement(By.linkText("Planos de Neg�cio")).click();
	    driver.findElement(By.name("menuSuperior:menuSuperior:j_idt28")).click();
	    driver.findElement(By.id("lista_planos:singleDT:2:visualizar")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.id("tabAnaliseMercado")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.id("formulario_cadastro_projeto:j_idt92")).click();
	    Thread.sleep(2000);
	    WebElement msgExemplo = driver.findElement(By.xpath("//*[@id=\"primefacesmessagedlg\"]/div[1]/span"));
	    Thread.sleep(2000);
	    String msgExemploSTR = msgExemplo.getText();
	    System.out.println(msgExemploSTR);
	    if(msgExemploSTR.contains("Ajuda com preenchimento de Rela��o com cliente")) {
	    	IntegracaoTestlinkSelenium.updateResults("relacoesCliente", "", TestLinkAPIResults.TEST_PASSED);
	    }else {
	    	IntegracaoTestlinkSelenium.updateResults("relacoesCliente", "", TestLinkAPIResults.TEST_FAILED);
	
	    }
	}

}
