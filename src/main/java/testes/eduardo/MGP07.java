package testes.eduardo;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

public class MGP07 {
	
	private WebDriver driver;

	@Before
	public void setUp() {
		
		System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://ggirardon.com:8080/GerenciadorPampatec/");
	}

	@Test
	public void testeSalvarDadosPlano() throws InterruptedException, TestLinkAPIException {
		
	    driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
	    driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("huillian15@gmail.com");
	    driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
	    driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("oioioi");
	    driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
	    driver.findElement(By.linkText("Planos de Neg�cio")).click();
	    driver.findElement(By.name("menuSuperior:menuSuperior:j_idt26")).click();
	    driver.findElement(By.id("formEquipe:botaoSalvar1")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
	    driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).clear();
	    driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).sendKeys("plano");
	    driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).clear();
	    driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).sendKeys("new");
	    driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).clear();
	    driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).sendKeys("new");
	    driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).clear();
	    driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).sendKeys("new");
	    driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar2")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.name("formulario_cadastro_projeto:j_idt57")).click();
	    driver.findElement(By.linkText("Op��es")).click();
	    driver.findElement(By.linkText("Sair")).click();
	    driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
	    driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("huillian15@gmail.com");
	    driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
	    driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("oioioi");
	    driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
	    driver.findElement(By.linkText("Planos de Neg�cio")).click();
	    driver.findElement(By.name("menuSuperior:menuSuperior:j_idt28")).click();
	    driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.id("tabAnaliseMercado")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.id("formulario_cadastro_projeto:relacoComClientes")).clear();
	    driver.findElement(By.id("formulario_cadastro_projeto:relacoComClientes")).sendKeys("new");
	    driver.findElement(By.id("formulario_cadastro_projeto:parceriasChaves")).clear();
	    driver.findElement(By.id("formulario_cadastro_projeto:parceriasChaves")).sendKeys("new");
	    driver.findElement(By.id("formulario_cadastro_projeto:canais")).clear();
	    driver.findElement(By.id("formulario_cadastro_projeto:canais")).sendKeys("new");
	    driver.findElement(By.id("formulario_cadastro_projeto:recursosPrincipais")).clear();
	    driver.findElement(By.id("formulario_cadastro_projeto:recursosPrincipais")).sendKeys("new");
	    driver.findElement(By.id("formulario_cadastro_projeto:concorrentes")).clear();
	    driver.findElement(By.id("formulario_cadastro_projeto:concorrentes")).sendKeys("teste edi��o");
	    driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar3")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.name("formulario_cadastro_projeto:j_idt57")).click();
	    Thread.sleep(2000);
	    WebElement campoEditado =  driver.findElement(By.id("formulario_cadastro_projeto:concorrentes"));
	    String campoEditadoSTR = campoEditado.getAttribute("value");
	    if(campoEditadoSTR.contains("teste edi��o")) {
	    	IntegracaoTestlinkSelenium.updateResults("salvarDados", "", TestLinkAPIResults.TEST_PASSED);
	    }else {
	    	IntegracaoTestlinkSelenium.updateResults("salvarDados", "", TestLinkAPIResults.TEST_FAILED);
	    }
	}
}
