package testes.eduardo;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import testlink.api.java.client.TestLinkAPIResults;

public class MGP01 {

	public static WebDriver driver;

	@Before
	public void setUp() {

		System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://ggirardon.com:8080/GerenciadorPampatec/");
	}

	public void testCriarPlano() throws Exception {

		try {
			WebElement inputEmail = driver.findElement(By.name("formularioDeLogin:emailInput"));
			inputEmail.sendKeys("huillian15@gmail.com");
			WebElement inputSenha = driver.findElement(By.name("formularioDeLogin:senhaInput"));
			inputSenha.sendKeys("oioioi");
			WebElement buttonLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
			buttonLogin.click();
			WebElement buttonMenuSuperior = driver
					.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[2]/a"));
			buttonMenuSuperior.click();
			WebElement buttonNovoPlano = driver
					.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[2]/ul/li[1]/input"));
			buttonNovoPlano.click();
			WebElement inputEquipe = driver.findElement(By.xpath("//*[@id=\"formEquipe:autocomplete_input\"]"));
			inputEquipe.sendKeys("huillian@hotmail.com");
			Thread.sleep(2000);
			WebElement adicionarEquipe = driver.findElement(By.xpath("//*[@id=\"formEquipe:j_idt203\"]/span[2]"));
			adicionarEquipe.click();
			WebElement salvarEquipe = driver.findElement(By.xpath("//*[@id=\"formEquipe:botaoSalvar1\"]/span[2]"));
			salvarEquipe.click();
			Thread.sleep(2000);
			WebElement confirmarEquipe = driver
					.findElement(By.xpath("//*[@id=\"modalInfoSalvarEquipe\"]/div/div/div[3]/input"));
			confirmarEquipe.click();
			// Neg�cio
			WebElement textNomePlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:empresaProjeto\"]"));
			textNomePlano.sendKeys("newPlano");
			WebElement textSeguimentoPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:segmentoDeClientes\"]"));
			textSeguimentoPlano.sendKeys("newPlano");
			WebElement textPropostaPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:propostaDeValor\"]"));
			textPropostaPlano.sendKeys("newPlano");
			WebElement textAtividadeChavePlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:atividadesChave\"]"));
			textAtividadeChavePlano.sendKeys("newPlano");
			// Analise de mercado
			WebElement analiseDeMercado = driver.findElement(By.xpath("//*[@id=\"tabAnaliseMercado\"]"));
			analiseDeMercado.click();
			Thread.sleep(2000);
			WebElement textRelacaoCliPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:relacoComClientes\"]"));
			textRelacaoCliPlano.sendKeys("newPlano");
			WebElement textParceriaChavePlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:parceriasChaves\"]"));
			textParceriaChavePlano.sendKeys("newPlano");
			WebElement textCanaisPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:canais\"]"));
			textCanaisPlano.sendKeys("newPlano");
			WebElement textRecursosPrincipaisPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:recursosPrincipais\"]"));
			textRecursosPrincipaisPlano.sendKeys("newPlano");
			WebElement textConcorrentesPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:concorrentes\"]"));
			textConcorrentesPlano.sendKeys("newPlano");
			WebElement produtoOuServico = driver.findElement(By.xpath("//*[@id=\"tabProdutoServico\"]"));
			produtoOuServico.click();
			Thread.sleep(2000);
			Select select = new Select(
					driver.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:estagioDeEvolucao\"]")));
			Thread.sleep(1000);
			select.selectByIndex(1);
			WebElement tecnologiaPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:tecnologiaProcessos\"]"));
			tecnologiaPlano.sendKeys("newPlano");
			WebElement potencialPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:potencialInovacaoTecnologica\"]"));
			potencialPlano.sendKeys("newPlano");
			WebElement aplicacaoPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:aplicacoes\"]"));
			aplicacaoPlano.sendKeys("newPlano");
			WebElement dificuldadesPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:dificuldadesEsperadas\"]"));
			dificuldadesPlano.sendKeys("newPlano");
			WebElement interacaoUniPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:interacaoEmpresaUniversidade\"]"));
			interacaoUniPlano.sendKeys("newPlano");
			WebElement interacaoGovPlano = driver.findElement(
					By.xpath("//*[@id=\"formulario_cadastro_projeto:interacaoEmpresaComunidadeGoverno\"]"));
			interacaoGovPlano.sendKeys("newPlano");
			WebElement infraestruturaPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:infraestrutura\"]"));
			infraestruturaPlano.sendKeys("newPlano");
			WebElement gestaoPessoas = driver.findElement(By.xpath("//*[@id=\"tabGestaoPessoas\"]"));
			gestaoPessoas.click();
			Thread.sleep(2000);
			WebElement acionistasPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:participacaoAcionaria\"]"));
			acionistasPlano.sendKeys("newPlano");
			WebElement potencialRendaEmpregoPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:potencialEmprego\"]"));
			potencialRendaEmpregoPlano.sendKeys("newPlano");
			WebElement planoFinanceiro = driver.findElement(By.xpath("//*[@id=\"tabPlanoFinanceiro\"]"));
			planoFinanceiro.click();
			Thread.sleep(2000);
			WebElement receitaPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:fontesDeReceita\"]"));
			receitaPlano.sendKeys("newPlano");

			WebElement custosPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:estruturaCustos\"]"));
			custosPlano.sendKeys("newPlano");

			WebElement investimentoInicialPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:investimentoInicial\"]"));
			investimentoInicialPlano.sendKeys("newPlano");

			WebElement buttonSubmeterPlano = driver.findElement(By.xpath("//*[@id=\"botao_submeter\"]"));
			buttonSubmeterPlano.click();

			WebElement buttonEnviarPlano = driver
					.findElement(By.xpath("//*[@id=\"form_enviar_projeto:j_idt221\"]/span[2]"));
			buttonEnviarPlano.click();

			Thread.sleep(2000);
			WebElement buttonConfirmarEnvioPlano = driver
					.findElement(By.xpath("//*[@id=\"modalInfoSubmeter\"]/div/div/div[3]/input"));
			buttonConfirmarEnvioPlano.click();
			IntegracaoTestlinkSelenium.updateResults("criar plano de neg�cio", "", TestLinkAPIResults.TEST_PASSED);
		} catch (Exception e) {

			IntegracaoTestlinkSelenium.updateResults("criar plano de neg�cio", "", TestLinkAPIResults.TEST_FAILED);
			e.getMessage();
		}
	}

	@Test
	public void testeExcluirPlano() throws Exception {

		WebElement inputEmail = driver.findElement(By.name("formularioDeLogin:emailInput"));
		inputEmail.sendKeys("huillian15@gmail.com");
		WebElement inputSenha = driver.findElement(By.name("formularioDeLogin:senhaInput"));
		inputSenha.sendKeys("oioioi");
		WebElement buttonLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
		buttonLogin.click();
		WebElement buttonMenuSuperior = driver
				.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[2]/a"));
		buttonMenuSuperior.click();
		Thread.sleep(2000);
		WebElement buttonListaPlano = driver
				.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[2]/ul/li[2]/input"));
		buttonListaPlano.click();
		Thread.sleep(2000);
		//precisa pegar o xpath do campo que deseja excluir
		WebElement buttonExcluirPlano = driver
				.findElement(By.xpath("//*[@id=\"lista_planos:singleDT:0:j_idt56\"]/span[2]"));
		buttonExcluirPlano.click();
		Thread.sleep(2000);
		WebElement buttonConfirmarExcluirPlano = driver
				.findElement(By.xpath("//*[@id=\"modalInfoDeExclusao\"]/div/div/div[3]/div/input"));
		buttonConfirmarExcluirPlano.click();
		Thread.sleep(2000);
		try {
			//precisa pegar o xpath do campo que deseja excluir
			WebElement teste = driver.findElement(By.xpath("//*[@id=\"lista_planos:singleDT:0:j_idt56\"]/span[2]"));
			IntegracaoTestlinkSelenium.updateResults("excluir plano de negocio", "", TestLinkAPIResults.TEST_FAILED);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			IntegracaoTestlinkSelenium.updateResults("excluir plano de negocio", "", TestLinkAPIResults.TEST_PASSED);
		}
	}

	public void testeConsultarPlano() throws Exception {
		driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
		driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("huillian15@gmail.com");
		driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
		driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("oioioi");
		driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
		Thread.sleep(1000);
		driver.findElement(By.linkText("Planos de Neg�cio")).click();
		driver.findElement(By.name("menuSuperior:menuSuperior:j_idt28")).click();
		driver.findElement(By.id("lista_planos:singleDT:globalFilter")).clear();
		driver.findElement(By.id("lista_planos:singleDT:globalFilter")).sendKeys("dudu");
		Thread.sleep(1000);
		driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
		WebElement textConsultarPlano = driver
				.findElement(By.xpath("//*[@id=\"lista_planos:singleDT_data\"]/tr/td[1]"));
		System.out.println(textConsultarPlano.getText());
		if (textConsultarPlano.getText().contains("dudu")) {
			IntegracaoTestlinkSelenium.updateResults("Consultar Plano", "", TestLinkAPIResults.TEST_PASSED);
		} else {
			IntegracaoTestlinkSelenium.updateResults("Consultar Plano", "", TestLinkAPIResults.TEST_FAILED);
		}
	}

}
