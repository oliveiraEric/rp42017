package testes.eduardo;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import suiteTestesSelenium.integracaoTestlinkSelenium;
import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIResults;

public class MGP08 {
	
	private WebDriver driver;
	
	@Before
	public void setUp() {
		
		System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://ggirardon.com:8080/GerenciadorPampatec/");
	}
	
	@Test
	public void testeCamposObrigatorios() throws Exception {

		
			WebElement inputEmail = driver.findElement(By.name("formularioDeLogin:emailInput"));
			inputEmail.sendKeys("huillian15@gmail.com");
			WebElement inputSenha = driver.findElement(By.name("formularioDeLogin:senhaInput"));
			inputSenha.sendKeys("oioioi");
			WebElement buttonLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
			buttonLogin.click();
			WebElement buttonMenuSuperior = driver
					.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[2]/a"));
			buttonMenuSuperior.click();
			WebElement buttonNovoPlano = driver
					.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[2]/ul/li[1]/input"));
			buttonNovoPlano.click();
			WebElement inputEquipe = driver.findElement(By.xpath("//*[@id=\"formEquipe:autocomplete_input\"]"));
			inputEquipe.sendKeys("huillian@hotmail.com");
			Thread.sleep(2000);
			WebElement adicionarEquipe = driver.findElement(By.xpath("//*[@id=\"formEquipe:j_idt203\"]/span[2]"));
			adicionarEquipe.click();
			Thread.sleep(2000);
			WebElement salvarEquipe = driver.findElement(By.xpath("//*[@id=\"formEquipe:botaoSalvar1\"]/span[2]"));
			salvarEquipe.click();
			Thread.sleep(2000);
			WebElement confirmarEquipe = driver
					.findElement(By.xpath("//*[@id=\"modalInfoSalvarEquipe\"]/div/div/div[3]/input"));
			confirmarEquipe.click();
			Thread.sleep(2000);
			// Neg�cio
			WebElement textNomePlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:empresaProjeto\"]"));
			textNomePlano.clear();
			WebElement textSeguimentoPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:segmentoDeClientes\"]"));
			textSeguimentoPlano.clear();
			WebElement textPropostaPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:propostaDeValor\"]"));
			textPropostaPlano.clear();
			WebElement textAtividadeChavePlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:atividadesChave\"]"));
			textAtividadeChavePlano.clear();
			// Analise de mercado
			WebElement analiseDeMercado = driver.findElement(By.xpath("//*[@id=\"tabAnaliseMercado\"]"));
			analiseDeMercado.click();
			Thread.sleep(2000);
			WebElement textRelacaoCliPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:relacoComClientes\"]"));
			textRelacaoCliPlano.clear();
			WebElement textParceriaChavePlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:parceriasChaves\"]"));
			textParceriaChavePlano.clear();
			WebElement textCanaisPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:canais\"]"));
			textCanaisPlano.clear();
			WebElement textRecursosPrincipaisPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:recursosPrincipais\"]"));
			textRecursosPrincipaisPlano.clear();
			WebElement textConcorrentesPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:concorrentes\"]"));
			textConcorrentesPlano.clear();
			WebElement produtoOuServico = driver.findElement(By.xpath("//*[@id=\"tabProdutoServico\"]"));
			produtoOuServico.click();
			Thread.sleep(2000);
			WebElement tecnologiaPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:tecnologiaProcessos\"]"));
			tecnologiaPlano.clear();
			WebElement potencialPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:potencialInovacaoTecnologica\"]"));
			potencialPlano.clear();
			WebElement aplicacaoPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:aplicacoes\"]"));
			aplicacaoPlano.clear();
			WebElement dificuldadesPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:dificuldadesEsperadas\"]"));
			dificuldadesPlano.clear();
			WebElement interacaoUniPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:interacaoEmpresaUniversidade\"]"));
			interacaoUniPlano.clear();
			WebElement interacaoGovPlano = driver.findElement(
					By.xpath("//*[@id=\"formulario_cadastro_projeto:interacaoEmpresaComunidadeGoverno\"]"));
			interacaoGovPlano.clear();
			WebElement infraestruturaPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:infraestrutura\"]"));
			infraestruturaPlano.clear();
			WebElement gestaoPessoas = driver.findElement(By.xpath("//*[@id=\"tabGestaoPessoas\"]"));
			gestaoPessoas.click();
			Thread.sleep(2000);
			WebElement acionistasPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:participacaoAcionaria\"]"));
			acionistasPlano.clear();
			WebElement potencialRendaEmpregoPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:potencialEmprego\"]"));
			potencialRendaEmpregoPlano.clear();
			WebElement planoFinanceiro = driver.findElement(By.xpath("//*[@id=\"tabPlanoFinanceiro\"]"));
			planoFinanceiro.click();
			Thread.sleep(2000);
			WebElement receitaPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:fontesDeReceita\"]"));
			receitaPlano.clear();
			WebElement custosPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:estruturaCustos\"]"));
			custosPlano.clear();
			WebElement investimentoInicialPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:investimentoInicial\"]"));
			investimentoInicialPlano.clear();
			WebElement buttonSubmeterPlano = driver.findElement(By.xpath("//*[@id=\"botao_submeter\"]"));
			buttonSubmeterPlano.click();
			WebElement buttonEnviarPlano = driver
					.findElement(By.xpath("//*[@id=\"form_enviar_projeto:j_idt221\"]/span[2]"));
			buttonEnviarPlano.click();
			Thread.sleep(2000);
			WebElement buttonConfirmarEnvioPlano = driver
					.findElement(By.xpath("//*[@id=\"modalInfoSubmeter\"]/div/div/div[3]/input"));
			buttonConfirmarEnvioPlano.click();
			driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[1]/a")).click();
			String url = driver.getCurrentUrl();
			if (url.contains("homeEmpreendedor.jsf")) {
				IntegracaoTestlinkSelenium.updateResults("camposIncompletos", "", TestLinkAPIClient.TEST_FAILED);
			} else {
				IntegracaoTestlinkSelenium.updateResults("camposIncompletos", "", TestLinkAPIClient.TEST_PASSED);
			}
	}

}

