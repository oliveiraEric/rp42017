package testes.eduardo;

import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;

public class integracaoTestlinkSelenium {

    public static final String TESTLINK_KEY = "86dde6a19eab28b884939af14cc575cc";
    public static final String TESTLIK_URL = "http://lesse.com.br/testlink/lib/api/xmlrpc/v1/xmlrpc.php";
    public static final String TEST_PROJECT_NAME = "Gerenciador Pampatec - Grupo 07";
    public static final String TEST_PLAN_NAME = "Gerenciador Pampatec Test Plan";
    public static final String BUILD_NAME = "Build01";

    public static void updateResults(String testCaseName, String exception, String results) throws TestLinkAPIException {
        TestLinkAPIClient testlink = new TestLinkAPIClient(TESTLINK_KEY, TESTLIK_URL);
        testlink.reportTestCaseResult(TEST_PROJECT_NAME, TEST_PLAN_NAME, testCaseName, BUILD_NAME, exception, results);
    }
}