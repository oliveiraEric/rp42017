package testes.eduardo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

public class MGP02 {

	private WebDriver driver;

	@Before
	public void setUp() {

		System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://ggirardon.com:8080/GerenciadorPampatec/");
	}

	public void testeCampoNomeLimiteCaracteres() throws InterruptedException, TestLinkAPIException {
		WebDriver driver = null;
		System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://192.168.56.101:8080/GerenciadorPampatec/");
		WebElement inputEmail = driver.findElement(By.name("formularioDeLogin:emailInput"));
		inputEmail.sendKeys("huillian15@gmail.com");
		WebElement inputSenha = driver.findElement(By.name("formularioDeLogin:senhaInput"));
		inputSenha.sendKeys("oioioi");
		WebElement buttonLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
		buttonLogin.click();
		WebElement menuSuperior = driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/a"));
		menuSuperior.click();
		WebElement buttonOpcaoMenu = driver
				.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/ul/li[1]/input"));
		buttonOpcaoMenu.click();
		WebElement inputNome = driver.findElement(By.name("formularioCadastro:nome"));
		inputNome.sendKeys(
				"oaoisjaoijsoiajsoiajdsoiadouahnosudhnqiueshiuqwhsoiahsiuqhwsouqahdiyaghsdoqhwsuiqgahsougaoiushqoiushgsiaugsiyagsiuagsiqysg");
		WebElement inputSenhaAtual = driver.findElement(By.name("formularioCadastro:senhaAtual"));
		inputSenhaAtual.sendKeys("oioioi");
		WebElement buttonSalvar = driver
				.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoFinalizarEdicao\"]/span[2]"));
		buttonSalvar.click();
		Thread.sleep(2000);
		WebElement buttonConfirmar = driver.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoConfirmar\"]"));
		buttonConfirmar.click();
		Thread.sleep(2000);
		WebElement inputNomeTeste = driver.findElement(By.name("formularioCadastro:nome"));
		if (inputNomeTeste.getAttribute("value").equalsIgnoreCase("dudu")) {
			IntegracaoTestlinkSelenium.updateResults("campoNomeEditarCadastroPessoal", "",
						TestLinkAPIResults.TEST_PASSED);
		} else {
				IntegracaoTestlinkSelenium.updateResults("campoNomeEditarCadastroPessoal", "",
						TestLinkAPIResults.TEST_FAILED);
		}
	}

	public void testeCampoTelefoneLimiteCaracteres() throws TestLinkAPIException, InterruptedException {
		WebDriver driver = null;
		System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://192.168.56.101:8080/GerenciadorPampatec/");
		WebElement inputEmail = driver.findElement(By.name("formularioDeLogin:emailInput"));
		inputEmail.sendKeys("huillian15@gmail.com");
		WebElement inputSenha = driver.findElement(By.name("formularioDeLogin:senhaInput"));
		inputSenha.sendKeys("oioioi");
		WebElement buttonLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
		buttonLogin.click();
		WebElement menuSuperior = driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/a"));
		menuSuperior.click();
		WebElement buttonOpcaoMenu = driver
				.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/ul/li[1]/input"));
		buttonOpcaoMenu.click();
		WebElement inputTelefone = driver.findElement(By.name("formularioCadastro:telefone"));
		inputTelefone.sendKeys("99999999999");
		WebElement inputSenhaAtual = driver.findElement(By.name("formularioCadastro:senhaAtual"));
		inputSenhaAtual.sendKeys("oioioi");
		WebElement buttonSalvar = driver
				.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoFinalizarEdicao\"]/span[2]"));
		buttonSalvar.click();
		Thread.sleep(2000);
		WebElement buttonConfirmar = driver.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoConfirmar\"]"));
		buttonConfirmar.click();
		Thread.sleep(2000);
		WebElement inputNomeTeste = driver.findElement(By.name("formularioCadastro:telefone"));
		if (inputNomeTeste.getAttribute("value").length() < 10) {
				IntegracaoTestlinkSelenium.updateResults("campoTelefoneMaxDigitosCadastroPessoal", "",
						TestLinkAPIResults.TEST_PASSED);
		} else {
				IntegracaoTestlinkSelenium.updateResults("campoTelefoneMaxDigitosCadastroPessoal", "",
						TestLinkAPIResults.TEST_FAILED);
		}
	}

	public void testeCampoTelefoneCaracterNaoNumerico() throws InterruptedException, TestLinkAPIException {
		WebDriver driver = null;
		System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://192.168.56.101:8080/GerenciadorPampatec/");

		WebElement inputEmail = driver.findElement(By.name("formularioDeLogin:emailInput"));
		inputEmail.sendKeys("huillian15@gmail.com");
		WebElement inputSenha = driver.findElement(By.name("formularioDeLogin:senhaInput"));
		inputSenha.sendKeys("oioioi");
		WebElement buttonLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
		buttonLogin.click();
		WebElement ListBox = driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/a"));
		ListBox.click();
		WebElement buttonOpcaoMenu = driver
				.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/ul/li[1]/input"));
		buttonOpcaoMenu.click();
		WebElement inputTelefone = driver.findElement(By.name("formularioCadastro:telefone"));
		inputTelefone.sendKeys("9999dd");
		WebElement inputSenhaAtual = driver.findElement(By.name("formularioCadastro:senhaAtual"));
		inputSenhaAtual.sendKeys("oioioi");
		WebElement buttonSalvar = driver
				.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoFinalizarEdicao\"]/span[2]"));
		buttonSalvar.click();
		Thread.sleep(2000);
		WebElement buttonConfirmar = driver.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoConfirmar\"]"));
		buttonConfirmar.click();
		Thread.sleep(2000);
		WebElement inputNomeTeste = driver.findElement(By.name("formularioCadastro:telefone"));
		if (inputNomeTeste.getAttribute("value").contains("dd")) {
				IntegracaoTestlinkSelenium.updateResults("campoTelefoneValoresNaoNumCadastroPessoal", "",
						TestLinkAPIResults.TEST_FAILED);
		} else {
				IntegracaoTestlinkSelenium.updateResults("campoTelefoneValoresNaoNumCadastroPessoal", "",
						TestLinkAPIResults.TEST_PASSED);
		}
	}

	public void testcampoTelefoneMaxDigitos() throws InterruptedException, TestLinkAPIException {
		
		WebElement inputEmail = driver.findElement(By.name("formularioDeLogin:emailInput"));
		inputEmail.sendKeys("huillian15@gmail.com");
		WebElement inputSenha = driver.findElement(By.name("formularioDeLogin:senhaInput"));
		inputSenha.sendKeys("oioioi");
		WebElement buttonLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
		buttonLogin.click();
		WebElement ListBox = driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/a"));
		ListBox.click();
		WebElement buttonOpcaoMenu = driver
				.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/ul/li[1]/input"));
		buttonOpcaoMenu.click();
		WebElement inputTelefone = driver.findElement(By.name("formularioCadastro:numero"));
		inputTelefone.sendKeys("99999999999");
		WebElement inputSenhaAtual = driver.findElement(By.name("formularioCadastro:senhaAtual"));
		inputSenhaAtual.sendKeys("oioioi");
		WebElement buttonSalvar = driver
				.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoFinalizarEdicao\"]/span[2]"));
		buttonSalvar.click();
		Thread.sleep(2000);
		WebElement buttonConfirmar = driver.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoConfirmar\"]"));
		buttonConfirmar.click();
		Thread.sleep(2000);
		WebElement inputNomeTeste = driver.findElement(By.name("formularioCadastro:telefone"));
		if (inputNomeTeste.getAttribute("value").length() < 10) {
				IntegracaoTestlinkSelenium.updateResults("campoTelefoneMaxDigitosCadastroPessoal", "",
						TestLinkAPIResults.TEST_PASSED);
		} else {
				IntegracaoTestlinkSelenium.updateResults("campoTelefoneMaxDigitosCadastroPessoal", "",
						TestLinkAPIResults.TEST_FAILED);
		}
	}
}
