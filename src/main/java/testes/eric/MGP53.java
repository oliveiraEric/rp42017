package testes.eric;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import testlink.api.java.client.TestLinkAPIClient;

public class MGP53 {

	public static WebDriver driver;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:/Users/Eric/Documents/Unipampa/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void testeAcessarSistema() throws Exception {
		driver.get("http://ggirardon.com:8080/GerenciadorPampatec/");
		WebElement email = driver.findElement(By.id("formularioDeLogin:emailInput"));
		email.clear();
		email.sendKeys("ericdsoliveira@gmail.com");
		WebElement senha = driver.findElement(By.id("formularioDeLogin:senhaInput"));
		senha.clear();
		senha.sendKeys("rpzinhodomal");
		WebElement botaoLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
		botaoLogin.click();
		Thread.sleep(6000);
		String url = driver.getCurrentUrl();
		if (url.contains("homeEmpreendedor.jsf")) {
			IntegracaoTestlink.updateResults("Acessar o sistema", "", TestLinkAPIClient.TEST_PASSED);
		} else {
			IntegracaoTestlink.updateResults("Acessar o sistema", "", TestLinkAPIClient.TEST_FAILED);
		}
		fazerLogout();
		Assert.assertTrue(url.contains("homeEmpreendedor.jsf"));
	}

	@Test
	public void testeVerificarSenha() throws Exception {
		driver.get("http://ggirardon.com:8080/GerenciadorPampatec/");
		WebElement email = driver.findElement(By.id("formularioDeLogin:emailInput"));
		email.clear();
		email.sendKeys("ericdsoliveira@gmail.com");
		WebElement senha = driver.findElement(By.id("formularioDeLogin:senhaInput"));
		senha.clear();
		senha.sendKeys("rpzinhodomal");
		boolean senhaNaoVisivel = Boolean.valueOf(senha.getAttribute("type").equals("password"));
		if (senhaNaoVisivel) {
			IntegracaoTestlink.updateResults("Verificar senha", "", TestLinkAPIClient.TEST_PASSED);
		} else {
			IntegracaoTestlink.updateResults("Verificar senha", "", TestLinkAPIClient.TEST_FAILED);
		}
		Assert.assertTrue(senhaNaoVisivel);
	}

	@Test
	public void testeEmailNaoCadastrado() throws Exception {
		driver.get("http://ggirardon.com:8080/GerenciadorPampatec/");
		WebElement email = driver.findElement(By.id("formularioDeLogin:emailInput"));
		email.clear();
		email.sendKeys("emailnaocadastrado@gmail.com");
		WebElement senha = driver.findElement(By.id("formularioDeLogin:senhaInput"));
		senha.clear();
		senha.sendKeys("teste");
		WebElement botaoLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
		botaoLogin.click();
		Thread.sleep(6000);
		WebElement erro = driver.findElement(By.xpath("//*[@id=\"formularioDeLogin\"]/div[1]/span"));
		String mensagemErro = erro.getText();
		if (mensagemErro.contains("Usu�rio ou Senha incorreto(s)")) {
			IntegracaoTestlink.updateResults("Verificar se dados nao conferem", "", TestLinkAPIClient.TEST_PASSED);
		} else {
			IntegracaoTestlink.updateResults("Verificar se dados nao conferem", "", TestLinkAPIClient.TEST_FAILED);
		}
		Assert.assertTrue(mensagemErro.contains("Usu�rio ou Senha incorreto(s)"));
	}

	@Test
	public void testeSenhaIncorreta() throws Exception {
		driver.get("http://ggirardon.com:8080/GerenciadorPampatec/");
		WebElement email = driver.findElement(By.id("formularioDeLogin:emailInput"));
		email.clear();
		email.sendKeys("ericdsoliveira@gmail.com");
		WebElement senha = driver.findElement(By.id("formularioDeLogin:senhaInput"));
		senha.clear();
		senha.sendKeys("minhasenha");
		WebElement botaoLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
		botaoLogin.click();
		Thread.sleep(6000);
		WebElement erro = driver.findElement(By.xpath("//*[@id=\"formularioDeLogin\"]/div[1]/span"));
		String mensagemErro = erro.getText();
		if (mensagemErro.contains("Usu�rio ou Senha incorreto(s)")) {
			IntegracaoTestlink.updateResults("Conferir se senha e incorreta", "", TestLinkAPIClient.TEST_PASSED);
		} else {
			IntegracaoTestlink.updateResults("Conferir se senha e incorreta", "", TestLinkAPIClient.TEST_FAILED);
		}
		Assert.assertTrue(mensagemErro.contains("Usu�rio ou Senha incorreto(s)"));
	}

	@Test
	public void testeCampoVazio() throws Exception {
		driver.get("http://ggirardon.com:8080/GerenciadorPampatec/");
		WebElement email = driver.findElement(By.id("formularioDeLogin:emailInput"));
		email.clear();
		email.sendKeys("ericdsoliveira@gmail.com");
		WebElement botaoLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
		botaoLogin.click();
		Thread.sleep(6000);
		WebElement erro = driver.findElement(By.xpath("//*[@id=\"formularioDeLogin\"]/div[1]/span"));
		String mensagemErro = erro.getText();
		if (mensagemErro.contains("Usu�rio ou Senha incorreto(s)")) {
			IntegracaoTestlink.updateResults("Verifica campo de login vazio", "", TestLinkAPIClient.TEST_PASSED);
		} else {
			IntegracaoTestlink.updateResults("Verifica campo de login vazio", "", TestLinkAPIClient.TEST_FAILED);
		}
		Assert.assertTrue(mensagemErro.contains("Usu�rio ou Senha incorreto(s)"));
	}

	public void fazerLogout() {
		WebElement menuOpcoes = driver.findElement(By.linkText("Op��es"));
		menuOpcoes.click();
		WebElement sair = driver.findElement(By.name("menuSuperior:menuSuperior:j_idt32"));
		sair.click();
	}

}