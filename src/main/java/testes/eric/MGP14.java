package testes.eric;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import testlink.api.java.client.TestLinkAPIClient;

public class MGP14 {

	public static WebDriver driver;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:/Users/Eric/Documents/Unipampa/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void testeRevisar() throws Exception {
		fazerLogin();
		WebElement menuPlanos = driver.findElement(By.linkText("Planos de Neg�cio"));
		menuPlanos.click();
		WebElement listaPlanos = driver.findElement(By.name("menuSuperior:menuSuperior:j_idt28"));
		listaPlanos.click();
		WebElement botaoVisualizar = driver.findElement(By.id("lista_planos:singleDT:0:visualizar"));
		botaoVisualizar.click();
		Thread.sleep(6000);
		WebElement botaoRevisar = driver.findElement(By.id("botao_revisar"));
		botaoRevisar.click();
		WebElement propostaDeValor = driver.findElement(By.id("form_revisao:revisao_proposta"));
		String propostaDeValorSTR = propostaDeValor.getText();
		if (propostaDeValorSTR.contains("Proposta de valor")) {
			IntegracaoTestlink.updateResults("Revisar Plano de Negocios", "", TestLinkAPIClient.TEST_PASSED);
		} else {
			IntegracaoTestlink.updateResults("Revisar Plano de Negocios", "", TestLinkAPIClient.TEST_FAILED);
		}
		fazerLogout();
		Assert.assertTrue(propostaDeValorSTR.contains("Proposta de valor"));
	}

	public void fazerLogin() throws Exception {
		driver.get("http://ggirardon.com:8080/GerenciadorPampatec/");
		WebElement email = driver.findElement(By.id("formularioDeLogin:emailInput"));
		email.clear();
		email.sendKeys("ericdsoliveira@gmail.com");
		WebElement senha = driver.findElement(By.id("formularioDeLogin:senhaInput"));
		senha.clear();
		senha.sendKeys("rpzinhodomal");
		WebElement botaoLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
		botaoLogin.click();
	}

	public void fazerLogout() {
		WebElement menuOpcoes = driver.findElement(By.linkText("Op��es"));
		menuOpcoes.click();
		WebElement sair = driver.findElement(By.name("menuSuperior:menuSuperior:j_idt32"));
		sair.click();
	}
}