/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testes.anderson;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 *
 * @author Flam1ng
 */
public class TestMGP73 {

    TestlinkConection com = new TestlinkConection();
    public String url = "http://ggirardon.com:8080/GerenciadorPampatec/";
    public WebDriver driver;

    @Before
    public void startAndLogin() throws MalformedURLException {
        System.setProperty("webdriver.chrome.driver", "src/drivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(url);
        driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("andersonsr2014@outlook.com");
        driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("B@h180817");
        driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();

    }

    @Test
    public void loginEmail() {
        try {
            //mudarEmailInvalido
            /*driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/a")).click();
            driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/ul/li[1]/input")).click();
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:email\"]")).clear();
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:email\"]")).sendKeys("naonaoanderson@gmail.comnaopode");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:senhaAtual\"]")).sendKeys("123456");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoFinalizarEdicao\"]/span[2]")).click();
            Thread.sleep(1000);
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoConfirmar\"]")).click();
            try {
                driver.findElement(By.xpath("//*[@id=\"formularioCadastro\"]/div[7]/div[2]/span"));

                try {
                    com.updateResults("mudarEmailInvalido", " ", TestLinkAPIResults.TEST_FAILED);
                } catch (TestLinkAPIException ex) {
                    Logger.getLogger(TestMGP73.class.getName()).log(Level.SEVERE, null, ex);
                }

            } catch (NoSuchElementException e) {
                try {
                    com.updateResults("mudarEmailInvalido", " ", TestLinkAPIResults.TEST_PASSED);
                } catch (TestLinkAPIException ex) {
                    Logger.getLogger(TestMGP73.class.getName()).log(Level.SEVERE, null, ex);
                }
            }*/
            //relogar

            driver.get(url);
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("andersonsr2014@outlook.com");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("B@h180817");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            // mesmo email
            driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/a")).click();
            driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/ul/li[1]/input")).click();
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:email\"]")).clear();
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:email\"]")).sendKeys("andersonsr2014@outlook.com");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:senhaAtual\"]")).sendKeys("B@h180817");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoFinalizarEdicao\"]/span[2]")).click();
            Thread.sleep(1000);
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoConfirmar\"]")).click();
            try {
                driver.findElement(By.xpath("//*[@id=\"formularioCadastro\"]/div[7]/div[2]/span"));
                try {
                    com.updateResults("mudarEmailMesmo", "", TestLinkAPIResults.TEST_PASSED);
                } catch (TestLinkAPIException ex) {
                    Logger.getLogger(TestMGP73.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (NoSuchElementException e) {
                try {
                    com.updateResults("mudarEmailMesmo", " ", TestLinkAPIResults.TEST_FAILED);
                } catch (TestLinkAPIException ex) {
                    Logger.getLogger(TestMGP73.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            //cadastroEmailJaUsado
            driver.get(url);
            driver.findElement(By.xpath("//*[@id=\"formularioDeCadastro:botaoContinuaCadastro\"]/span[2]")).click();
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:nome\"]")).sendKeys("joao marmota");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:cpf\"]")).sendKeys("854.732.960-92");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:telefone\"]")).sendKeys("5599991234");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:j_idt19\"]")).sendKeys("ninja da vila da folha");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:j_idt21\"]")).sendKeys("chunin");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:rua\"]")).sendKeys("alameda dos anjos");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:numero\"]")).sendKeys("12345");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:bairro\"]")).sendKeys("cansei");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:j_idt33\"]")).sendKeys("apt 101");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:email\"]")).sendKeys("andersonsr2014@outlook.com");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:senha\"]")).sendKeys("chavedeacesso1");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:senhaConfig\"]")).sendKeys("chavedeacesso1");
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoEnviar\"]/span[2]")).click();
            Thread.sleep(30000);
            try {
                driver.findElement(By.xpath("//*[@id=\"formularioCadastro\"]/div[12]/span"));
                try {
                    com.updateResults("cadastroEmailJaUsado", " ", TestLinkAPIResults.TEST_PASSED);
                } catch (TestLinkAPIException ex) {
                    Logger.getLogger(TestMGP73.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (NoSuchElementException e) {
                try {
                    com.updateResults("cadastroEmailJaUsado", " ", TestLinkAPIResults.TEST_FAILED);
                } catch (TestLinkAPIException ex) {
                    Logger.getLogger(TestMGP73.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } catch (InterruptedException ex) {
            Logger.getLogger(TestMGP73.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void testeEmailInvalido() {
        driver.get(url);
        driver.findElement(By.xpath("//*[@id=\"formularioDeCadastro:botaoContinuaCadastro\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"formularioCadastro:nome\"]")).sendKeys("jose pedro");
        driver.findElement(By.xpath("//*[@id=\"formularioCadastro:cpf\"]")).sendKeys("542.449.670-95");
        driver.findElement(By.xpath("//*[@id=\"formularioCadastro:telefone\"]")).sendKeys("5599991234");
        driver.findElement(By.xpath("//*[@id=\"formularioCadastro:j_idt19\"]")).sendKeys("ninja da vila da folha");
        driver.findElement(By.xpath("//*[@id=\"formularioCadastro:j_idt21\"]")).sendKeys("chunin");
        driver.findElement(By.xpath("//*[@id=\"formularioCadastro:rua\"]")).sendKeys("alameda dos anjos");
        driver.findElement(By.xpath("//*[@id=\"formularioCadastro:numero\"]")).sendKeys("12345");
        driver.findElement(By.xpath("//*[@id=\"formularioCadastro:bairro\"]")).sendKeys("cansei");
        driver.findElement(By.xpath("//*[@id=\"formularioCadastro:j_idt33\"]")).sendKeys("apt 101");
        driver.findElement(By.xpath("//*[@id=\"formularioCadastro:email\"]")).sendKeys("andersonsr2014@outlook.como");
        driver.findElement(By.xpath("//*[@id=\"formularioCadastro:senha\"]")).sendKeys("chavedeacesso1");
        driver.findElement(By.xpath("//*[@id=\"formularioCadastro:senhaConfig\"]")).sendKeys("chavedeacesso1");
        driver.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoEnviar\"]/span[2]")).click();
        try {
            driver.findElement(By.xpath("//*[@id=\"formularioCadastro\"]/div[12]/span"));
            try {
                com.updateResults("cadastrarEmailInvalido", " ", TestLinkAPIResults.TEST_PASSED);
            } catch (TestLinkAPIException ex) {
                Logger.getLogger(TestMGP73.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (NoSuchElementException e) {
            try {
                com.updateResults("cadastrarEmailInvalido", " ", TestLinkAPIResults.TEST_FAILED);
            } catch (TestLinkAPIException ex) {
                Logger.getLogger(TestMGP73.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
