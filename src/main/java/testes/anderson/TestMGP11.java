/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testes.anderson;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 *
 * @author Flam1ng
 */
public class TestMGP11 {

    TestlinkConection com = new TestlinkConection();
    public String url = "http://192.168.56.101:8080/GerenciadorPampatec/";
    public WebDriver driver;

    @Before
    public void startAndLogin() throws MalformedURLException {
        System.setProperty("webdriver.chrome.driver", "src/drivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(url);
        driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("andersonsr2014@outlook.com");
        driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("B@h060293");
        driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();

    }

    @Test
    public void testEquipe() throws TestLinkAPIException {
        try {

            driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[2]/a")).click();
            Thread.sleep(500);
            driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[2]/ul/li[1]/input")).click();
            //excluir correspondente

            //teste emailInvalido
            driver.findElement(By.id("formEquipe:autocomplete_input")).sendKeys("jejeje.com");
            driver.findElement(By.xpath("//*[@id=\"formEquipe:j_idt203\"]/span[2]")).click();
            Thread.sleep(500);
            try {
                driver.findElement(By.id("formEquipe:messagemErroEquipe"));
                com.updateResults("emailInvalido", "", TestLinkAPIResults.TEST_FAILED);
            } catch (NoSuchElementException e) {
                com.updateResults("emailInvalido", e.getMessage(), TestLinkAPIResults.TEST_PASSED);
            }
            //teste emailValido
            driver.findElement(By.id("formEquipe:autocomplete_input")).clear();
            driver.findElement(By.id("formEquipe:autocomplete_input")).sendKeys("andyedarosasf@hotmail.com");
            driver.findElement(By.xpath("//*[@id=\"formEquipe:j_idt203\"]/span[2]")).click();
            Thread.sleep(500);
            try {
                driver.findElement(By.xpath("//*[@id=\"formEquipe:tabelaEmpreendedores_data\"]/tr[2]/td[1]"));
                com.updateResults("emailValido", " ", TestLinkAPIResults.TEST_PASSED);
            } catch (NoSuchElementException e) {
                com.updateResults("emailValido", e.getMessage(), TestLinkAPIResults.TEST_FAILED);
            }
            Thread.sleep(500);
            //teste emailDuplicado
            driver.findElement(By.id("formEquipe:autocomplete_input")).clear();
            driver.findElement(By.id("formEquipe:autocomplete_input")).sendKeys("andyedarosasf@hotmail.com");
            driver.findElement(By.xpath("//*[@id=\"formEquipe:j_idt203\"]/span[2]")).click();
            Thread.sleep(500);
            try {
                driver.findElement(By.id("formEquipe:mensagemErroEquipe"));
                com.updateResults("emailDuplicado", "", TestLinkAPIResults.TEST_PASSED);
            } catch (NoSuchElementException e) {
                com.updateResults("emailDuplicado", e.getMessage(), TestLinkAPIResults.TEST_FAILED);
            }
            //teste excluirObservador
            driver.findElement(By.xpath("//*[@id=\"formEquipe:tabelaEmpreendedores:1:botaoExcluirEmpreendedor\"]/span[2]")).click();
            Thread.sleep(500);
            if (driver.findElement(By.xpath("//*[@id=\"formEquipe:tabelaEmpreendedores_data\"]/tr[1]/td[1]")).getText() == "andyedarosasf@hotmail.com") {
                com.updateResults("excluirObservador", " ", TestLinkAPIResults.TEST_FAILED);
            } else {
                com.updateResults("excluirObservador", " ", TestLinkAPIResults.TEST_PASSED);
            }
            //teste confirmarSalvar
            driver.findElement(By.xpath("//*[@id=\"formEquipe:botaoSalvar1\"]/span[2]")).click();
            Thread.sleep(500);
            try {
                com.updateResults("confirmarSalvar", " ", TestLinkAPIResults.TEST_PASSED);
            } catch (NoSuchElementException e) {
                com.updateResults("confirmarSalvar", e.getMessage(), TestLinkAPIResults.TEST_FAILED);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(TestMGP11.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
