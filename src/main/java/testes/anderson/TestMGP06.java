/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testes.anderson;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 *
 * @author Flam1ng
 */
public class TestMGP06 {

	TestlinkConection com = new TestlinkConection();
	public String url = "http://192.168.56.101:8080/GerenciadorPampatec/";
	public WebDriver driver;

	@Before
	public void startAndLogin() throws MalformedURLException {
		System.setProperty("webdriver.chrome.driver", "src/drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(url);
		driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("andersonsr2014@outlook.com");
		driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("B@h060293");
		driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();

	}

	@Test
	public void testeSegmentoCliente() throws TestLinkAPIException {

		driver.findElements(By.className("aComoBotaoMenu")).get(1).click();
		driver.findElement(By.name("menuSuperior:menuSuperior:j_idt26")).click();
		driver.findElement(By.id("formEquipe:botaoSalvar1")).click();
		while (!driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).isDisplayed()) {

		}
		driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
		driver.findElement(By.id("formulario_cadastro_projeto:j_idt78")).click();
		try {
			driver.findElement(By.id("primefacesmessagedlg"));
			driver.close();
			com.updateResults("seguimentoCliente", "", TestLinkAPIResults.TEST_PASSED);
		} catch (NoSuchElementException e) {
			com.updateResults("seguimentoCliente", "NoSuchElementException", TestLinkAPIResults.TEST_FAILED);
		}
	}

	@Test
	public void testePropostaValor() throws TestLinkAPIException {

		driver.findElements(By.className("aComoBotaoMenu")).get(1).click();
		driver.findElement(By.name("menuSuperior:menuSuperior:j_idt26")).click();
		driver.findElement(By.id("formEquipe:botaoSalvar1")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException ex) {
			Logger.getLogger(TestMGP06.class.getName()).log(Level.SEVERE, null, ex);
		}
		driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
		driver.findElement(By.id("formulario_cadastro_projeto:j_idt82")).click();
		try {
			driver.findElement(By.id("primefacesmessagedlg"));
			driver.close();
			com.updateResults("propostaValor", "", TestLinkAPIResults.TEST_PASSED);
		} catch (NoSuchElementException e) {
			com.updateResults("propostaValor", "NoSuchElementException", TestLinkAPIResults.TEST_FAILED);
		}

	}

	@Test
	public void testeAtividadesChave() throws TestLinkAPIException {

		driver.findElements(By.className("aComoBotaoMenu")).get(1).click();
		driver.findElement(By.name("menuSuperior:menuSuperior:j_idt26")).click();
		driver.findElement(By.id("formEquipe:botaoSalvar1")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException ex) {
			Logger.getLogger(TestMGP06.class.getName()).log(Level.SEVERE, null, ex);
		}
		driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
		driver.findElement(By.id("formulario_cadastro_projeto:j_idt86")).click();
		try {
			driver.findElement(By.id("primefacesmessagedlg"));
			driver.close();
			com.updateResults("atividadesChave", "", TestLinkAPIResults.TEST_PASSED);
		} catch (NoSuchElementException e) {
			com.updateResults("atividadesChave", "NoSuchElementException", TestLinkAPIResults.TEST_FAILED);
		}
	}

	@Test
	public void testeRelacoesCliente() throws TestLinkAPIException {

		try {

			driver.findElements(By.className("aComoBotaoMenu")).get(1).click();
			driver.findElement(By.name("menuSuperior:menuSuperior:j_idt26")).click();
			driver.findElement(By.id("formEquipe:botaoSalvar1")).click();
			Thread.sleep(2000);
			driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
			driver.findElement(By.id("tabAnaliseMercado")).click();
			Thread.sleep(2000);
			driver.findElement(By.id("formulario_cadastro_projeto:j_idt92")).click();
			try {
				driver.findElement(By.id("primefacesmessagedlg"));
				driver.close();
				com.updateResults("relacoesCliente", "", TestLinkAPIResults.TEST_PASSED);
			} catch (NoSuchElementException e) {
				com.updateResults("relacoesCliente", "NoSuchElementException", TestLinkAPIResults.TEST_FAILED);
			}

		} catch (InterruptedException ex) {
			Logger.getLogger(TestMGP06.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	@Test
	public void testeDescricao() throws TestLinkAPIException {
		driver.findElements(By.className("aComoBotaoMenu")).get(1).click();
		driver.findElement(By.name("menuSuperior:menuSuperior:j_idt26")).click();
		driver.findElement(By.id("formEquipe:botaoSalvar1")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException ex) {
			Logger.getLogger(TestMGP06.class.getName()).log(Level.SEVERE, null, ex);
		}
		driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
		driver.findElement(By.id("tabAnaliseMercado")).click();
		try {
			driver.findElement(By.xpath("//*[@id=\"formularioParte03\"]/div[5]/text()"));
			driver.close();
			com.updateResults("descricaoConcorrentes", "", TestLinkAPIResults.TEST_PASSED);
		} catch (NoSuchElementException e) {
			com.updateResults("descricaoConcorrentes", "NoSuchElementException", TestLinkAPIResults.TEST_FAILED);
		}

	}
}
