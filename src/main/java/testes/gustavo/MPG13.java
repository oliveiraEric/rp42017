/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testes.gustavo;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import testes.eric.IntegracaoTestlink;
import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 *
 * @author Windows
 */
public class MPG13 {

    private static WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    public MPG13() {
    }

    public static final String TESTLINK_KEY = "f93c7e0b858985c1fd1beef6ff6bb384";
    public static final String TESTLINK_URL = "http://www.lesse.com.br/testlink/lib/api/xmlrpc/v1/xmlrpc.php";
    public static final String TESTLINK_PROJECT_NAME = "Gerenciador Pampatec - Grupo 7";
    public static final String TEST_PLAN_NAME = "Gerenciador Pampatec Test Plan";
//    public static final String TEST_CASE_NAME = "";
    public static final String BUILD_NAME = "Build01";

    public static void updateResults(String testCaseName, String exception, String results) throws TestLinkAPIException {
        TestLinkAPIClient testLink = new TestLinkAPIClient(TESTLINK_KEY, TESTLINK_URL);
        testLink.reportTestCaseResult(TESTLINK_PROJECT_NAME, TEST_PLAN_NAME, testCaseName, BUILD_NAME, exception, results);

    }

    @Before
    public void setUp() {
        
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Windows\\Desktop\\Engenharia de Software\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("ggirardon.com:8080/GerenciadorPampatec");

    }

    @Test
    public void teste1MPG13() throws TestLinkAPIException {
//        try{
        WebElement element = driver.findElement(By.name("formularioDeLogin:emailInput"));
        element.sendKeys("gugs_costacarvalho@hotmail.com");
        element = driver.findElement(By.name("formularioDeLogin:senhaInput"));
        element.sendKeys("senha123");
        driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/homeEmpreendedor.jsf");
        driver.findElement(By.linkText("Planos de Negócio")).click();
        driver.findElement(By.name("menuSuperior:menuSuperior:j_idt26")).click();
        driver.findElement(By.id("formEquipe:botaoSalvar1")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
        driver.findElement(By.id("tabNegocio")).click();
        driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).sendKeys("Neque porro quisquam.");
        driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet");
        driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar2")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.name("formulario_cadastro_projeto:j_idt57")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.id("tabNegocio")).click();
        driver.findElement(By.id("tabAnaliseMercado")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.id("formulario_cadastro_projeto:relacoComClientes")));
        driver.findElement(By.id("formulario_cadastro_projeto:relacoComClientes")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:relacoComClientes")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:parceriasChaves")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:parceriasChaves")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:canais")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:canais")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:recursosPrincipais")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:recursosPrincipais")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:concorrentes")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:concorrentes")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.name("formulario_cadastro_projeto:botaoSalvar3")).click();
        WebElement elementoo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")));
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.id("tabProdutoServico")).click();
        WebElement elemento2 = wait.until(ExpectedConditions.elementToBeClickable(By.id("formulario_cadastro_projeto:estagioDeEvolucao")));
        new Select(driver.findElement(By.id("formulario_cadastro_projeto:estagioDeEvolucao"))).selectByVisibleText("Projeto Detalhado");
        driver.findElement(By.id("formulario_cadastro_projeto:tecnologiaProcessos")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:tecnologiaProcessos")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:potencialInovacaoTecnologica")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:potencialInovacaoTecnologica")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:aplicacoes")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:aplicacoes")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:dificuldadesEsperadas")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:dificuldadesEsperadas")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaUniversidade")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaUniversidade")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaComunidadeGoverno")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaComunidadeGoverno")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:infraestrutura")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:infraestrutura")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar4")).click();
        WebElement elemento3 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")));
        driver.findElement(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.id("tabGestaoPessoas")).click();
        WebElement elemento4 = wait.until(ExpectedConditions.elementToBeClickable(By.id("formulario_cadastro_projeto:participacaoAcionaria")));
        driver.findElement(By.id("formulario_cadastro_projeto:participacaoAcionaria")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:participacaoAcionaria")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:potencialEmprego")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:potencialEmprego")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar5")).click();
        WebElement elemento5 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")));
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.id("tabPlanoFinanceiro")).click();
        WebElement elemento6 = wait.until(ExpectedConditions.elementToBeClickable(By.id("formulario_cadastro_projeto:fontesDeReceita")));
        driver.findElement(By.id("formulario_cadastro_projeto:fontesDeReceita")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:fontesDeReceita")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:estruturaCustos")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:estruturaCustos")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:investimentoInicial")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:investimentoInicial")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar6")).click();
        WebElement element0 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")));
        driver.findElement(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.xpath("//*[@id=\"botao_submeter\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"form_enviar_projeto:j_idt221\"]/span[2]")).click();
        WebElement elementoC = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalInfoSubmeter\"]/div/div/div[3]/input")));
        driver.findElement(By.xpath("//*[@id=\"modalInfoSubmeter\"]/div/div/div[3]/input")).click();
        driver.findElement(By.linkText("Planos de Negócio")).click();
        driver.findElement(By.linkText("Lista de Planos")).click();
        new Select(driver.findElement(By.name("lista_planos:singleDT:j_idt48"))).selectByVisibleText("Todos");
        
        driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/paginaBuscaPlanoDeNegocio.jsf");
        WebElement id = driver.findElement(By.xpath("(\"//*[@id=\\\"sessao_plano_nao_avaliado\\\"]/h1"));
        String id1 = id.getText();
        if (statusValidoWorkflow(id1)) {
            IntegracaoTestlink.updateResults("teste1MPG13", null, TestLinkAPIResults.TEST_PASSED);
        } else {
            IntegracaoTestlink.updateResults("teste1MPG13", null, TestLinkAPIResults.TEST_FAILED);
        }

    }

    @Test
    public void teste2MPG13() throws TestLinkAPIException {
        WebElement element = driver.findElement(By.name("formularioDeLogin:emailInput"));
        element.sendKeys("gugs_costacarvalho@hotmail.com");
        element = driver.findElement(By.name("formularioDeLogin:senhaInput"));
        element.sendKeys("senha123");
        driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/homeEmpreendedor.jsf");
        driver.findElement(By.linkText("Planos de Negócio")).click();
        driver.findElement(By.name("menuSuperior:menuSuperior:j_idt26")).click();
        driver.findElement(By.id("formEquipe:botaoSalvar1")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
        driver.findElement(By.id("tabNegocio")).click();
        driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).sendKeys("Neque porro quisquam.");
        driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet");
        driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar2")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.name("formulario_cadastro_projeto:j_idt57")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.id("tabNegocio")).click();
        driver.findElement(By.id("tabAnaliseMercado")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.id("formulario_cadastro_projeto:relacoComClientes")));
        driver.findElement(By.id("formulario_cadastro_projeto:relacoComClientes")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:relacoComClientes")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:parceriasChaves")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:parceriasChaves")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:canais")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:canais")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:recursosPrincipais")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:recursosPrincipais")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:concorrentes")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:concorrentes")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.name("formulario_cadastro_projeto:botaoSalvar3")).click();
        WebElement elementoo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")));
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.id("tabProdutoServico")).click();
        WebElement elemento2 = wait.until(ExpectedConditions.elementToBeClickable(By.id("formulario_cadastro_projeto:estagioDeEvolucao")));
        new Select(driver.findElement(By.id("formulario_cadastro_projeto:estagioDeEvolucao"))).selectByVisibleText("Projeto Detalhado");
        driver.findElement(By.id("formulario_cadastro_projeto:tecnologiaProcessos")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:tecnologiaProcessos")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:potencialInovacaoTecnologica")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:potencialInovacaoTecnologica")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:aplicacoes")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:aplicacoes")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:dificuldadesEsperadas")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:dificuldadesEsperadas")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaUniversidade")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaUniversidade")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaComunidadeGoverno")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaComunidadeGoverno")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:infraestrutura")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:infraestrutura")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar4")).click();
        WebElement elemento3 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")));
        driver.findElement(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.id("tabGestaoPessoas")).click();
        WebElement elemento4 = wait.until(ExpectedConditions.elementToBeClickable(By.id("formulario_cadastro_projeto:participacaoAcionaria")));
        driver.findElement(By.id("formulario_cadastro_projeto:participacaoAcionaria")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:participacaoAcionaria")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:potencialEmprego")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:potencialEmprego")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar5")).click();
        WebElement elemento5 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")));
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.id("tabPlanoFinanceiro")).click();
        WebElement elemento6 = wait.until(ExpectedConditions.elementToBeClickable(By.id("formulario_cadastro_projeto:fontesDeReceita")));
        driver.findElement(By.id("formulario_cadastro_projeto:fontesDeReceita")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:fontesDeReceita")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:estruturaCustos")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:estruturaCustos")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:investimentoInicial")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:investimentoInicial")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar6")).click();
        WebElement element0 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")));
        driver.findElement(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.xpath("//*[@id=\"botao_submeter\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"form_enviar_projeto:j_idt221\"]/span[2]")).click();
        WebElement elementoC = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalInfoSubmeter\"]/div/div/div[3]/input")));
        driver.findElement(By.xpath("//*[@id=\"modalInfoSubmeter\"]/div/div/div[3]/input")).click();
        driver.findElement(By.linkText("Planos de Negócio")).click();
        driver.findElement(By.linkText("Lista de Planos")).click();
        new Select(driver.findElement(By.name("lista_planos:singleDT:j_idt48"))).selectByVisibleText("Todos");

//        WebElement id = driver.findElement(By.xpath("*[@id=\"lista_planos:singleDT_data\"]/tr[1]/td[4]"));
//        new Select(driver.findElement(By.name("lista_planos:singleDT:j_idt48"))).selectByVisibleText("Em pré-avaliação");
        driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/paginaBuscaPlanoDeNegocio.jsf");
        WebElement id = driver.findElement(By.xpath("(\"//*[@id=\\\"sessao_plano_nao_avaliado\\\"]/h1"));
        String id1 = id.getText();
        if (statusValidoWorkflow(id1)) {
            IntegracaoTestlink.updateResults("teste2MPG13", null, TestLinkAPIResults.TEST_PASSED);
        } else {
            IntegracaoTestlink.updateResults("teste2MPG13", null, TestLinkAPIResults.TEST_FAILED);
        }

    }
    
    @Test
    public void teste3MPG13() throws TestLinkAPIException {
        WebElement element = driver.findElement(By.name("formularioDeLogin:emailInput"));
        element.sendKeys("gugs_costacarvalho@hotmail.com");
        element = driver.findElement(By.name("formularioDeLogin:senhaInput"));
        element.sendKeys("senha123");
        driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/homeEmpreendedor.jsf");
        driver.findElement(By.linkText("Planos de Negócio")).click();
        driver.findElement(By.name("menuSuperior:menuSuperior:j_idt26")).click();
        driver.findElement(By.id("formEquipe:botaoSalvar1")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
        driver.findElement(By.id("tabNegocio")).click();
        driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).sendKeys("Neque porro quisquam.");
        driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet");
        driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar2")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.name("formulario_cadastro_projeto:j_idt57")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.id("tabNegocio")).click();
        driver.findElement(By.id("tabAnaliseMercado")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        WebElement elemento = wait.until(ExpectedConditions.elementToBeClickable(By.id("formulario_cadastro_projeto:relacoComClientes")));
        driver.findElement(By.id("formulario_cadastro_projeto:relacoComClientes")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:relacoComClientes")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:parceriasChaves")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:parceriasChaves")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:canais")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:canais")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:recursosPrincipais")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:recursosPrincipais")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:concorrentes")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:concorrentes")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.name("formulario_cadastro_projeto:botaoSalvar3")).click();
        WebElement elementoo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")));
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.id("tabProdutoServico")).click();
        WebElement elemento2 = wait.until(ExpectedConditions.elementToBeClickable(By.id("formulario_cadastro_projeto:estagioDeEvolucao")));
        new Select(driver.findElement(By.id("formulario_cadastro_projeto:estagioDeEvolucao"))).selectByVisibleText("Projeto Detalhado");
        driver.findElement(By.id("formulario_cadastro_projeto:tecnologiaProcessos")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:tecnologiaProcessos")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:potencialInovacaoTecnologica")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:potencialInovacaoTecnologica")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:aplicacoes")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:aplicacoes")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:dificuldadesEsperadas")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:dificuldadesEsperadas")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaUniversidade")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaUniversidade")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaComunidadeGoverno")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaComunidadeGoverno")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:infraestrutura")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:infraestrutura")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar4")).click();
        WebElement elemento3 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")));
        driver.findElement(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.id("tabGestaoPessoas")).click();
        WebElement elemento4 = wait.until(ExpectedConditions.elementToBeClickable(By.id("formulario_cadastro_projeto:participacaoAcionaria")));
        driver.findElement(By.id("formulario_cadastro_projeto:participacaoAcionaria")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:participacaoAcionaria")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:potencialEmprego")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:potencialEmprego")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar5")).click();
        WebElement elemento5 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")));
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.id("tabPlanoFinanceiro")).click();
        WebElement elemento6 = wait.until(ExpectedConditions.elementToBeClickable(By.id("formulario_cadastro_projeto:fontesDeReceita")));
        driver.findElement(By.id("formulario_cadastro_projeto:fontesDeReceita")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:fontesDeReceita")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:estruturaCustos")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:estruturaCustos")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:investimentoInicial")).clear();
        driver.findElement(By.id("formulario_cadastro_projeto:investimentoInicial")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.");
        driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar6")).click();
        WebElement element0 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")));
        driver.findElement(By.xpath("//*[@id=\"modalInfoSalvar\"]/div/div/div[3]/input")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/enviarProjeto.jsf");
        driver.findElement(By.xpath("//*[@id=\"botao_submeter\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"form_enviar_projeto:j_idt221\"]/span[2]")).click();
        WebElement elementoC = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"modalInfoSubmeter\"]/div/div/div[3]/input")));
        driver.findElement(By.xpath("//*[@id=\"modalInfoSubmeter\"]/div/div/div[3]/input")).click();
        driver.findElement(By.linkText("Planos de Negócio")).click();
        driver.findElement(By.linkText("Lista de Planos")).click();
        new Select(driver.findElement(By.name("lista_planos:singleDT:j_idt48"))).selectByVisibleText("Todos");

        driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/paginaBuscaPlanoDeNegocio.jsf");
        WebElement id = driver.findElement(By.xpath("(\"//*[@id=\\\"sessao_plano_nao_avaliado\\\"]/h1"));
        String id1 = id.getText();
        if (statusValidoWorkflow(id1)) {
            IntegracaoTestlink.updateResults("teste2MPG13", null, TestLinkAPIResults.TEST_PASSED);
        } else {
            IntegracaoTestlink.updateResults("teste2MPG13", null, TestLinkAPIResults.TEST_FAILED);
        }

    }

    /**
     *
     * @throws Exception
     */
    private boolean statusValidoWorkflow(String status) {
        if (status.equals("Em elaboração") || status.equals("Necessita melhoria")
                || status.equals("Aceito para avaliação") || status.equals("Em formalização")
                || status.equals("Incubação") || status.equals("Reprovado")) {
            return true;
        }
        return false;

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
