/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testes.gustavo;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 *
 * @author Gustavo
 */
public class MPG54 {

    private static WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Windows\\Desktop\\Engenharia de Software\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("ggirardon.com:8080/GerenciadorPampatec");

    }

//    @Test
//    public void teste1MPG54() throws TestLinkAPIException, InterruptedException {
//        //caso de teste simples que checa um plano de neg�cio em status "NECESSITA MELHORIA"
//        //e adiciona novas informa��es nos campos que requerem modifica��es
//        WebElement element = driver.findElement(By.name("formularioDeLogin:emailInput"));
//        element.sendKeys("gugs_costacarvalho@hotmail.com");
//        element = driver.findElement(By.name("formularioDeLogin:senhaInput"));
//        element.sendKeys("senha123");
//          driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
//        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/homeEmpreendedor.jsf");
//        driver.findElement(By.linkText("Planos de Neg�cio")).click();
//        driver.findElement(By.name("menuSuperior:menuSuperior:j_idt28")).click();
//        new Select(driver.findElement(By.name("lista_planos:singleDT:j_idt48"))).selectByVisibleText("Necessita melhoria");
//        driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
//        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/planoDeNegocio/revisarPlanoDeNegocio.jsf");
//
//        //foram adicionados coment�rios em 3 tabs: negocio, analise de mercado e
//        //gest�o de pessoas - sendo verificado o coment�rio antes de add coisas
//        //novas nos campos de resubme��o
//        driver.findElement(By.id("menuSuperior:botao_revisar")).click();
//        driver.findElement(By.id("tabNegocio")).click();
//        driver.findElement(By.id("formulario_resubmeterplano:comentarioSegCliente")).click();
//        driver.findElement(By.id("formulario_resubmeterplano:segmentoDeClientes1")).clear();
//        driver.findElement(By.id("formulario_resubmeterplano:segmentoDeClientes1")).sendKeys("Neque porro quisquam. Coment�rio do gerente teste adicionado");
//        driver.findElement(By.id("tabAnaliseMercado")).click();
//        driver.findElement(By.id("formulario_resubmeterplano:comentarioRecursosPrincipais")).click();
//        driver.findElement(By.id("formulario_resubmeterplano:recursosPrincipais1")).clear();
//        driver.findElement(By.id("formulario_resubmeterplano:recursosPrincipais1")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet. Coment�rio do gerente teste adicionado");
//        driver.findElement(By.id("tabGestaoPessoas")).click();
//        driver.findElement(By.id("formulario_resubmeterplano:comentarioPotencialEmpregoRenda")).click();
//        driver.findElement(By.id("formulario_resubmeterplano:potencialEmprego1")).clear();
//        driver.findElement(By.id("formulario_resubmeterplano:potencialEmprego1")).sendKeys("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet. Coment�rio do gerente teste adicionado");
//        try {
//            driver.findElement(By.id("menuSuperior:botao_Ressubmeter")).click();
//            driver.findElement(By.cssSelector("div.modal-dialog > div.modal-dialog > div.modal-content > div.modal-footer > button.btn.btn-default")).click();
//            driver.findElement(By.id("menuSuperior:botao_Ressubmeter")).click();
//            Thread.sleep(3000);
//            driver.findElement(By.name("formulario_resubmeterplano:j_idt54")).click();
//            IntegracaoTestlink.updateResults("NOMECASODETESTE", " ", TestLinkAPIResults.TEST_PASSED);
//        } catch (Exception e) {
//            IntegracaoTestlink.updateResults("NOMECASODETESTE", " ", TestLinkAPIResults.TEST_FAILED);
//        }
//    }
//    @Test
//    public void teste2MPG54() throws TestLinkAPIException, InterruptedException {
//        //caso de teste que checa um plano de neg�cio em status "ACEITO PARA AVALIA��O"
//        //e que verifica a mensagem padr�o pr�-definida -- OK
//        WebElement element = driver.findElement(By.name("formularioDeLogin:emailInput"));
//        element.sendKeys("gugs_costacarvalho@hotmail.com");
//        element = driver.findElement(By.name("formularioDeLogin:senhaInput"));
//        element.sendKeys("senha123");
//        driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
//        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/homeEmpreendedor.jsf");
//        driver.findElement(By.linkText("Planos de Neg�cio")).click();
//        driver.findElement(By.name("menuSuperior:menuSuperior:j_idt28")).click();
//        new Select(driver.findElement(By.name("lista_planos:singleDT:j_idt48"))).selectByVisibleText("Aceito para avalia��o");
//        driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
//        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/planoDeNegocio/revisarPlanoDeNegocio.jsf");
//
//        WebElement id = driver.findElement(By.id("observacoes2"));
//        String id1 = id.getText();
//        if (id1.startsWith("O seu plano de neg�cio for aprovado.")) {
//            IntegracaoTestlink.updateResults("NOMEDOTESTE", " ", TestLinkAPIResults.TEST_PASSED);
//
//        } else {
//            IntegracaoTestlink.updateResults("NOMEDOTESTE", " ", TestLinkAPIResults.TEST_FAILED);
//
//        }
    
//    @Test
//    public void teste3MPG54() throws TestLinkAPIException, InterruptedException {
//        //caso de teste que checa um plano de neg�cio em status "REPROVADO"
//        //e que verifica a mensagem padr�o pr�-definida - OK
//        WebElement element = driver.findElement(By.name("formularioDeLogin:emailInput"));
//        element.sendKeys("gugs_costacarvalho@hotmail.com");
//        element = driver.findElement(By.name("formularioDeLogin:senhaInput"));
//        element.sendKeys("senha123");
//        driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
//        
//        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/homeEmpreendedor.jsf");
//        driver.findElement(By.linkText("Planos de Neg�cio")).click();
//        driver.findElement(By.name("menuSuperior:menuSuperior:j_idt28")).click();
//        new Select(driver.findElement(By.name("lista_planos:singleDT:j_idt48"))).selectByVisibleText("Reprovado");
//        driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
//        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/planoDeNegocio/revisarPlanoDeNegocio.jsf");
//
//        WebElement id = driver.findElement(By.id("observacoes2"));
//        String id1 = id.getText();
//        if (id1.startsWith("O seu plano de neg�cio foi reprovado.")) {
//            IntegracaoTestlink.updateResults("NOMEDOTESTE", " ", TestLinkAPIResults.TEST_PASSED);
//        } else {
//            IntegracaoTestlink.updateResults("NOMEDOTESTE", " ", TestLinkAPIResults.TEST_FAILED);
//        }
//    }
//
//    
    public void teste4MPG54(){
        //caso de teste que o gerente comenta em todos os campos dispon�veis e
        //manda para reavalia��o para o empreendedor
        WebElement element = driver.findElement(By.name("formularioDeLogin:emailInput"));
        element.sendKeys("gugs_costacarvalho@hotmail.com");
        element = driver.findElement(By.name("formularioDeLogin:senhaInput"));
        element.sendKeys("senha123");

        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/homeEmpreendedor.jsf");
        driver.findElement(By.linkText("Planos de Neg�cio")).click();
        driver.findElement(By.name("menuSuperior:menuSuperior:j_idt28")).click();
        new Select(driver.findElement(By.name("lista_planos:singleDT:j_idt48"))).selectByVisibleText("Necessita melhoria");
        driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
        driver.get(baseUrl + "http://ggirardon.com:8080/GerenciadorPampatec/view/empreendedor/planoDeNegocio/revisarPlanoDeNegocio.jsf");
        
        driver.findElement(By.id("menuSuperior:botao_revisar")).click();
        WebElement aba = driver.findElement(By.xpath("abaPlano"));
        String cor = aba.getCssValue("#ff0000");
        
    }

}
