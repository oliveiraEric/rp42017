package eduardo;

import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

public class MGP02 {

	public static final String TESTLINK_KEY = "a04ea8b9e8c47f2cc622b5d2201bd5b2";
	public static final String TESTLIK_URL = "http://lesse.com.br/testlink/lib/api/xmlrpc/v1/xmlrpc.php";
	public static final String TEST_PROJECT_NAME = "Gerenciador Pampatec - Grupo 07";
	public static final String TEST_PLAN_NAME = "Gerenciador Pampatec Test Plan";
	public static final String BUILD_NAME = "Build01";
	public static WebDriver driver;
	public void updateResults(String testCaseName, String exception, String results) throws TestLinkAPIException {
		TestLinkAPIClient testlink = new TestLinkAPIClient(TESTLINK_KEY, TESTLIK_URL);

		testlink.reportTestCaseResult(TEST_PROJECT_NAME, TEST_PLAN_NAME, testCaseName, BUILD_NAME, exception, results);

	}

	@Before
	public void driver() {
		
		System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://ggirardon.com:8080/GerenciadorPampatec/");
	}
	
	public void test() {
		WebDriver driver = null;
		System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://192.168.56.101:8080/GerenciadorPampatec/");

		WebElement inputEmail = driver.findElement(By.name("formularioDeLogin:emailInput"));
		inputEmail.sendKeys("huillian15@gmail.com");
		WebElement inputSenha = driver.findElement(By.name("formularioDeLogin:senhaInput"));
		inputSenha.sendKeys("oioioi");
		WebElement buttonLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
		buttonLogin.click();
		WebElement menuSuperior = driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/a"));
		menuSuperior.click();
		WebElement buttonOpcaoMenu = driver
				.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/ul/li[1]/input"));
		buttonOpcaoMenu.click();
		WebElement inputNome = driver.findElement(By.name("formularioCadastro:nome"));
		inputNome.sendKeys(
				"oaoisjaoijsoiajsoiajdsoiadouahnosudhnqiueshiuqwhsoiahsiuqhwsouqahdiyaghsdoqhwsuiqgahsougaoiushqoiushgsiaugsiyagsiuagsiqysg");
		WebElement inputSenhaAtual = driver.findElement(By.name("formularioCadastro:senhaAtual"));
		inputSenhaAtual.sendKeys("oioioi");
		WebElement buttonSalvar = driver
				.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoFinalizarEdicao\"]/span[2]"));
		buttonSalvar.click();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		WebElement buttonConfirmar = driver.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoConfirmar\"]"));
		buttonConfirmar.click();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		WebElement inputNomeTeste = driver.findElement(By.name("formularioCadastro:nome"));
		if (inputNomeTeste.getAttribute("value").equalsIgnoreCase("dudu")) {
			try {
				updateResults("campoNomeEditarCadastroPessoal", "", TestLinkAPIResults.TEST_PASSED);
			} catch (TestLinkAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				updateResults("campoNomeEditarCadastroPessoal", "", TestLinkAPIResults.TEST_FAILED);
			} catch (TestLinkAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	
	public void test01() {
		WebDriver driver = null;
		System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://192.168.56.101:8080/GerenciadorPampatec/");

		WebElement inputEmail = driver.findElement(By.name("formularioDeLogin:emailInput"));
		inputEmail.sendKeys("huillian15@gmail.com");
		WebElement inputSenha = driver.findElement(By.name("formularioDeLogin:senhaInput"));
		inputSenha.sendKeys("oioioi");
		WebElement buttonLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
		buttonLogin.click();
		WebElement menuSuperior = driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/a"));
		menuSuperior.click();
		WebElement buttonOpcaoMenu = driver
				.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/ul/li[1]/input"));
		buttonOpcaoMenu.click();
		WebElement inputTelefone = driver.findElement(By.name("formularioCadastro:telefone"));
		inputTelefone.sendKeys("99999999999");
		WebElement inputSenhaAtual = driver.findElement(By.name("formularioCadastro:senhaAtual"));
		inputSenhaAtual.sendKeys("oioioi");
		WebElement buttonSalvar = driver
				.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoFinalizarEdicao\"]/span[2]"));
		buttonSalvar.click();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		WebElement buttonConfirmar = driver.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoConfirmar\"]"));
		buttonConfirmar.click();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement inputNomeTeste = driver.findElement(By.name("formularioCadastro:telefone"));
		if (inputNomeTeste.getAttribute("value").length() < 10) {
			try {
				updateResults("campoTelefoneMaxDigitosCadastroPessoal", "", TestLinkAPIResults.TEST_PASSED);
			} catch (TestLinkAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				updateResults("campoTelefoneMaxDigitosCadastroPessoal", "", TestLinkAPIResults.TEST_FAILED);
			} catch (TestLinkAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	
	public void test02() {
		WebDriver driver = null;
		System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://192.168.56.101:8080/GerenciadorPampatec/");

		WebElement inputEmail = driver.findElement(By.name("formularioDeLogin:emailInput"));
		inputEmail.sendKeys("huillian15@gmail.com");
		WebElement inputSenha = driver.findElement(By.name("formularioDeLogin:senhaInput"));
		inputSenha.sendKeys("oioioi");
		WebElement buttonLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
		buttonLogin.click();
		WebElement ListBox = driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/a"));
		ListBox.click();
		WebElement buttonOpcaoMenu = driver
				.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/ul/li[1]/input"));
		buttonOpcaoMenu.click();
		WebElement inputTelefone = driver.findElement(By.name("formularioCadastro:telefone"));
		inputTelefone.sendKeys("9999dd");
		WebElement inputSenhaAtual = driver.findElement(By.name("formularioCadastro:senhaAtual"));
		inputSenhaAtual.sendKeys("oioioi");
		WebElement buttonSalvar = driver
				.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoFinalizarEdicao\"]/span[2]"));
		buttonSalvar.click();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		WebElement buttonConfirmar = driver.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoConfirmar\"]"));
		buttonConfirmar.click();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		WebElement inputNomeTeste = driver.findElement(By.name("formularioCadastro:telefone"));

		if (inputNomeTeste.getAttribute("value").contains("dd")) {
			try {
				updateResults("campoTelefoneValoresNaoNumCadastroPessoal", "", TestLinkAPIResults.TEST_FAILED);
			} catch (TestLinkAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				updateResults("campoTelefoneValoresNaoNumCadastroPessoal", "", TestLinkAPIResults.TEST_PASSED);
			} catch (TestLinkAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	
	public void test03() {
		WebElement inputEmail = driver.findElement(By.name("formularioDeLogin:emailInput"));
		inputEmail.sendKeys("huillian15@gmail.com");
		WebElement inputSenha = driver.findElement(By.name("formularioDeLogin:senhaInput"));
		inputSenha.sendKeys("oioioi");
		WebElement buttonLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
		buttonLogin.click();
		WebElement ListBox = driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/a"));
		ListBox.click();
		WebElement buttonOpcaoMenu = driver
				.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[3]/ul/li[1]/input"));
		buttonOpcaoMenu.click();
		WebElement inputTelefone = driver.findElement(By.name("formularioCadastro:numero"));
		inputTelefone.sendKeys("99999999999");
		WebElement inputSenhaAtual = driver.findElement(By.name("formularioCadastro:senhaAtual"));
		inputSenhaAtual.sendKeys("oioioi");
		WebElement buttonSalvar = driver
				.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoFinalizarEdicao\"]/span[2]"));
		buttonSalvar.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		WebElement buttonConfirmar = driver.findElement(By.xpath("//*[@id=\"formularioCadastro:botaoConfirmar\"]"));
		buttonConfirmar.click();

		try {
			Thread.sleep(4000);

		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		WebElement inputNomeTeste = driver.findElement(By.name("formularioCadastro:telefone"));

		if (inputNomeTeste.getAttribute("value").length() < 10) {

			try {

				updateResults("campoTelefoneMaxDigitosCadastroPessoal", "", TestLinkAPIResults.TEST_PASSED);

			} catch (TestLinkAPIException e) {
				e.printStackTrace();
			}

		} else {

			try {

				updateResults("campoTelefoneMaxDigitosCadastroPessoal", "", TestLinkAPIResults.TEST_FAILED);

			} catch (TestLinkAPIException e) {
				e.printStackTrace();
			}

		}

	}
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
}

