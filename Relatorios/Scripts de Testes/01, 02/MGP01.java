package eduardo;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

public class MGP01 {

	public static final String TESTLINK_KEY = "a04ea8b9e8c47f2cc622b5d2201bd5b2";
	public static final String TESTLIK_URL = "http://lesse.com.br/testlink/lib/api/xmlrpc/v1/xmlrpc.php";
	public static final String TEST_PROJECT_NAME = "Gerenciador Pampatec - Grupo 07";
	public static final String TEST_PLAN_NAME = "Gerenciador Pampatec Test Plan";
	public static final String BUILD_NAME = "Build01";
	public WebDriver driver;

	public void updateResults(String testCaseName, String exception, String results) throws TestLinkAPIException {
		TestLinkAPIClient testlink = new TestLinkAPIClient(TESTLINK_KEY, TESTLIK_URL);

		testlink.reportTestCaseResult(TEST_PROJECT_NAME, TEST_PLAN_NAME, testCaseName, BUILD_NAME, exception, results);

	}

	@Before
	public void driver() {

		System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://ggirardon.com:8080/GerenciadorPampatec/");
	}

	public void testCriarPlano() {

		try {
			WebElement inputEmail = driver.findElement(By.name("formularioDeLogin:emailInput"));
			inputEmail.sendKeys("huillian15@gmail.com");
			WebElement inputSenha = driver.findElement(By.name("formularioDeLogin:senhaInput"));
			inputSenha.sendKeys("oioioi");
			WebElement buttonLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
			buttonLogin.click();

			WebElement buttonMenuSuperior = driver
					.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[2]/a"));
			buttonMenuSuperior.click();
			WebElement buttonNovoPlano = driver
					.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[2]/ul/li[1]/input"));
			buttonNovoPlano.click();

			WebElement inputEquipe = driver.findElement(By.xpath("//*[@id=\"formEquipe:autocomplete_input\"]"));
			inputEquipe.sendKeys("huillian@hotmail.com");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			WebElement adicionarEquipe = driver.findElement(By.xpath("//*[@id=\"formEquipe:j_idt203\"]/span[2]"));
			adicionarEquipe.click();

			WebElement salvarEquipe = driver.findElement(By.xpath("//*[@id=\"formEquipe:botaoSalvar1\"]/span[2]"));
			salvarEquipe.click();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			WebElement confirmarEquipe = driver
					.findElement(By.xpath("//*[@id=\"modalInfoSalvarEquipe\"]/div/div/div[3]/input"));
			confirmarEquipe.click();

			// Neg�cio
			WebElement textNomePlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:empresaProjeto\"]"));
			textNomePlano.sendKeys("newPlano");
			WebElement textSeguimentoPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:segmentoDeClientes\"]"));
			textSeguimentoPlano.sendKeys("newPlano");

			WebElement textPropostaPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:propostaDeValor\"]"));
			textPropostaPlano.sendKeys("newPlano");

			WebElement textAtividadeChavePlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:atividadesChave\"]"));
			textAtividadeChavePlano.sendKeys("newPlano");

			// Analise de mercado
			WebElement analiseDeMercado = driver.findElement(By.xpath("//*[@id=\"tabAnaliseMercado\"]"));
			analiseDeMercado.click();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			WebElement textRelacaoCliPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:relacoComClientes\"]"));
			textRelacaoCliPlano.sendKeys("newPlano");
			WebElement textParceriaChavePlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:parceriasChaves\"]"));
			textParceriaChavePlano.sendKeys("newPlano");
			WebElement textCanaisPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:canais\"]"));
			textCanaisPlano.sendKeys("newPlano");
			WebElement textRecursosPrincipaisPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:recursosPrincipais\"]"));
			textRecursosPrincipaisPlano.sendKeys("newPlano");
			WebElement textConcorrentesPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:concorrentes\"]"));
			textConcorrentesPlano.sendKeys("newPlano");
			WebElement produtoOuServico = driver.findElement(By.xpath("//*[@id=\"tabProdutoServico\"]"));
			produtoOuServico.click();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Select select = new Select(
					driver.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:estagioDeEvolucao\"]")));
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			select.selectByIndex(1);

			WebElement tecnologiaPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:tecnologiaProcessos\"]"));
			tecnologiaPlano.sendKeys("newPlano");

			WebElement potencialPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:potencialInovacaoTecnologica\"]"));
			potencialPlano.sendKeys("newPlano");

			WebElement aplicacaoPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:aplicacoes\"]"));
			aplicacaoPlano.sendKeys("newPlano");

			WebElement dificuldadesPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:dificuldadesEsperadas\"]"));
			dificuldadesPlano.sendKeys("newPlano");

			WebElement interacaoUniPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:interacaoEmpresaUniversidade\"]"));
			interacaoUniPlano.sendKeys("newPlano");

			WebElement interacaoGovPlano = driver.findElement(
					By.xpath("//*[@id=\"formulario_cadastro_projeto:interacaoEmpresaComunidadeGoverno\"]"));
			interacaoGovPlano.sendKeys("newPlano");

			WebElement infraestruturaPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:infraestrutura\"]"));
			infraestruturaPlano.sendKeys("newPlano");

			WebElement gestaoPessoas = driver.findElement(By.xpath("//*[@id=\"tabGestaoPessoas\"]"));
			gestaoPessoas.click();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			WebElement acionistasPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:participacaoAcionaria\"]"));
			acionistasPlano.sendKeys("newPlano");

			WebElement potencialRendaEmpregoPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:potencialEmprego\"]"));
			potencialRendaEmpregoPlano.sendKeys("newPlano");

			WebElement planoFinanceiro = driver.findElement(By.xpath("//*[@id=\"tabPlanoFinanceiro\"]"));
			planoFinanceiro.click();

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			WebElement receitaPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:fontesDeReceita\"]"));
			receitaPlano.sendKeys("newPlano");

			WebElement custosPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:estruturaCustos\"]"));
			custosPlano.sendKeys("newPlano");

			WebElement investimentoInicialPlano = driver
					.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:investimentoInicial\"]"));
			investimentoInicialPlano.sendKeys("newPlano");

			WebElement buttonSubmeterPlano = driver.findElement(By.xpath("//*[@id=\"botao_submeter\"]"));
			buttonSubmeterPlano.click();

			WebElement buttonEnviarPlano = driver
					.findElement(By.xpath("//*[@id=\"form_enviar_projeto:j_idt221\"]/span[2]"));
			buttonEnviarPlano.click();

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			WebElement buttonConfirmarEnvioPlano = driver
					.findElement(By.xpath("//*[@id=\"modalInfoSubmeter\"]/div/div/div[3]/input"));
			buttonConfirmarEnvioPlano.click();
			updateResults("criar plano de neg�cio", "", TestLinkAPIResults.TEST_PASSED);

		} catch (Exception e) {
			try {
				updateResults("criar plano de neg�cio", "", TestLinkAPIResults.TEST_FAILED);
			} catch (TestLinkAPIException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.getMessage();
		}

	}
	@Test
	public void test05ExcluirPlano() {
		

		WebElement inputEmail = driver.findElement(By.name("formularioDeLogin:emailInput"));
		inputEmail.sendKeys("huillian15@gmail.com");
		WebElement inputSenha = driver.findElement(By.name("formularioDeLogin:senhaInput"));
		inputSenha.sendKeys("oioioi");
		WebElement buttonLogin = driver.findElement(By.id("formularioDeLogin:botaoLogin"));
		buttonLogin.click();

		WebElement buttonMenuSuperior = driver
				.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[2]/a"));
		buttonMenuSuperior.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement buttonListaPlano = driver
				.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[2]/ul/li[2]/input"));
		buttonListaPlano.click();

		WebElement buttonExcluirPlano = driver
				.findElement(By.xpath("//*[@id=\"lista_planos:singleDT:2:j_idt56\"]/span[2]"));
		buttonExcluirPlano.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		WebElement buttonConfirmarExcluirPlano = driver
				.findElement(By.xpath("//*[@id=\"modalInfoDeExclusao\"]/div/div/div[3]/div/input"));
		buttonConfirmarExcluirPlano.click();

		WebElement teste = driver.findElement(By.xpath("//*[@id=\"lista_planos:singleDT:2:j_idt56\"]/span[2]"));
		if (teste.isDisplayed()) {
			try {

				updateResults("excluir plano de negocio", "", TestLinkAPIResults.TEST_FAILED);

			} catch (TestLinkAPIException e) {
				e.printStackTrace();
			}
		} else {
			try {

				updateResults("excluir plano de negocio", "", TestLinkAPIResults.TEST_PASSED);

			} catch (TestLinkAPIException e) {
				e.printStackTrace();
			}
		}

	}
	
	public void testExcluirPlano() {
		

		driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
		driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("huillian15@gmail.com");
		driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
		driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("oioioi");
		driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
		driver.findElement(By.linkText("Planos de Neg�cio")).click();
		driver.findElement(By.name("menuSuperior:menuSuperior:j_idt28")).click();
		driver.findElement(By.id("lista_planos:singleDT:0:j_idt56")).click();
		driver.findElement(By.name("lista_planos:singleDT:0:j_idt58")).click();

	}

	
	public void testeConsultarPlano7() throws TestLinkAPIException {

			driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
			driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("huillian15@gmail.com");
			driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
			driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("oioioi");
			driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			driver.findElement(By.linkText("Planos de Neg�cio")).click();
			driver.findElement(By.name("menuSuperior:menuSuperior:j_idt28")).click();
			driver.findElement(By.id("lista_planos:singleDT:globalFilter")).clear();
			driver.findElement(By.id("lista_planos:singleDT:globalFilter")).sendKeys("dudu");
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
			
			// driver.findElement(By.linkText("In�cio")).click();
			// WebElement Inicio = driver.findElement(By.linkText("In�cio"));
			// Assert.assertTrue(Inicio.isDisplayed());
			
			WebElement textConsultarPlano = driver.findElement(By.xpath("//*[@id=\"lista_planos:singleDT_data\"]/tr/td[1]"));
			System.out.println(textConsultarPlano.getText());
			
			if (textConsultarPlano.getText().contains("dudu")) {
				
				updateResults("Consultar Plano", "", TestLinkAPIResults.TEST_PASSED);
			} else {
				updateResults("Consultar Plano", "", TestLinkAPIResults.TEST_FAILED);
			}

		

	}
}
