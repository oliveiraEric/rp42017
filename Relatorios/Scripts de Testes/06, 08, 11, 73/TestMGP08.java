/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RPIVTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;
import testlinkIntegration.TestlinkConection;

/**
 *
 * @author Flam1ng
 */
public class TestMGP08 {

    TestlinkConection com = new TestlinkConection();
    public String url = "http://192.168.56.101:8080/GerenciadorPampatec/";
    public WebDriver driver;

    @Before
    public void startAndLogin() throws MalformedURLException {
        System.setProperty("webdriver.chrome.driver", "src/drivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(url);
        driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("andersonsr2014@outlook.com");
        driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("B@h060293");
        driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();

    }

    @Test
    public void testeCamposIncompleto() {
        try {
            driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[2]/a")).click();
            driver.findElement(By.name("menuSuperior:menuSuperior:j_idt26")).click();
            driver.findElement(By.id("formEquipe:autocomplete_input")).sendKeys("jose@hotmail.com");
            driver.findElement(By.xpath("//*[@id=\"formEquipe:j_idt203\"]/span[2]")).click();
            driver.findElement(By.xpath("//*[@id=\"formEquipe:botaoSalvar1\"]/span[2]")).click();
            Thread.sleep(2000);
            driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
            driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).sendKeys("nerds gordos e sedentarios");
            driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).sendKeys("diversão");
            driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).sendKeys("joguinho besta online");
            driver.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:botaoSalvar2\"]/span[2]")).click();
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:botaoSalvar2\"]/span[2]")).submit();
            Thread.sleep(2000);
            driver.findElement(By.id("botao_submeter")).click();
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[@id=\"form_enviar_projeto:j_idt221\"]/span[2]")).click();
            Thread.sleep(2000);
            driver.findElement(By.name("formulario_cadastro_projeto:j_idt61")).click();
            driver.findElement(By.id("tabAnaliseMercado")).click();
            Thread.sleep(2000);
            if (driver.findElement(By.id("formulario_cadastro_projeto:mensagemErroRelacoes")).isDisplayed()) {
                try {
                    com.updateResults("camposIncompleto", " ", TestLinkAPIResults.TEST_PASSED);
                } catch (TestLinkAPIException ex) {
                    Logger.getLogger(TestMGP08.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    com.updateResults("camposIncompleto", "SalvouComCampoEmBranco", TestLinkAPIResults.TEST_FAILED);
                } catch (TestLinkAPIException ex) {
                    Logger.getLogger(TestMGP08.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(TestMGP08.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void teste() {
        try {
            driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[2]/a")).click();
            driver.findElement(By.xpath("//*[@id=\"menuSuperior\"]/nav/div/div[2]/ul/li[2]/ul/li[1]/input")).click();
            driver.findElement(By.xpath("//*[@id=\"formEquipe:botaoSalvar1\"]/span[2]")).click();
            Thread.sleep(2000);
            driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
            driver.findElement(By.id("tabProdutoServico")).click();
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:estagioDeEvolucao\"]")).click();
            Thread.sleep(500);
            driver.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:estagioDeEvolucao\"]/option[7]")).click();
            driver.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:botaoSalvar4\"]/span[2]")).click();
            Thread.sleep(500);
            driver.findElement(By.name("formulario_cadastro_projeto:j_idt57")).click();
            driver.findElement(By.name("botao_submeter")).click();
            driver.findElement(By.xpath("//*[@id=\"form_enviar_projeto:j_idt221\"]/span[2]")).click();
            Thread.sleep(500);
            driver.findElement(By.name("formulario_cadastro-projeto:j_idt61")).click();
            driver.findElement(By.id("tabprodutoServico")).click();
            Thread.sleep(500);
            if (driver.findElement(By.xpath("//*[@id=\"formulario_cadastro_projeto:estagioDeEvolucao\"]")).getText().equals("outros")) {
                try {
                    com.updateResults("estagioEvolucao", " ", TestLinkAPIResults.TEST_PASSED);
                } catch (TestLinkAPIException ex) {
                    Logger.getLogger(TestMGP08.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    com.updateResults("estagioEvolucao", "", TestLinkAPIResults.TEST_FAILED);
                } catch (TestLinkAPIException ex) {
                    Logger.getLogger(TestMGP08.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } catch (InterruptedException ex) {
            Logger.getLogger(TestMGP08.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
