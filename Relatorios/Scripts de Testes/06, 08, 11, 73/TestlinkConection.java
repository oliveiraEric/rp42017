/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testlinkIntegration;
import java.util.logging.Level;
import java.util.logging.Logger;
import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;
/**
 *
 * @author Flam1ng
 */
public class TestlinkConection {
    public static final String KEY = "469f6c911898542c819dee36ccfe6a06";
    public static final String URL = "http://lesse.com.br/testlink/lib/api/xmlrpc/v1/xmlrpc.php";
    public static final String BUILD = "Build01";
    public static final String PLAN = "Gerenciador Pampatec Test Plan";
    public static final String PROJECT = "Gerenciador Pampatec - Grupo 07";
    
    public void updateResults(String testCase, String exception, String result) throws TestLinkAPIException {
        TestLinkAPIClient client = new TestLinkAPIClient(KEY, URL);
     
            client.reportTestCaseResult(PROJECT, PLAN, testCase, BUILD, exception, result);
      
    }
}
